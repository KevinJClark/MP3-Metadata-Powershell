# BEFORE YOU RUN THIS: Please download and extract this assembly that is required for PowerShell to work with MP3 metadata.
# http://download.banshee.fm/taglib-sharp/2.1.0.0/taglib-sharp-2.1.0.0-windows.zip
#
# References: http://www.toddklindt.com/MP3TagswithPowerShell
# http://www.powershelladmin.com/wiki/PowerShell_foreach_loops_and_ForEach-Object
# http://www.powershelladmin.com/wiki/Powershell_split_operator
# https://thelonedba.wordpress.com/2016/09/11/powershell-slicing/
# http://stackoverflow.com/questions/12923074/how-to-load-assemblies-in-powershell
#
# Created and tested in PowerShell CONSOLE version 4.0
#
# An example of what each of the variables should look like:
# $MusicDir: "C:\Users\Kevin\Desktop\Music"
# $FileName: "Seven Lions - Keep It Close (Feat Kerli).mp3","Direct - Tranquility (Audile Remix).mp3"
# $Title: "Keep It Close (Feat Kerli)","Tranquility (Audile Remix)"
# $Artist: "Seven Lions","Direct"
# $Featured: "Kerli"
# $Remixer: "Audile"

Write-Output "This script takes a directory of mp3 files and fills in metadata with the artist, title, and optionally the feature and remixer.`n"


$AssemblyPath = Read-Host -Prompt "Please give the path of the taglib-sharp.dll file"
$AssemblyData = [IO.File]::ReadAllBytes($AssemblyPath) # Read in the assembly
[Reflection.Assembly]::Load($AssemblyData) | Out-Null # and then load it into Powershell

$MusicDir =  Resolve-Path (Read-Host -Prompt "Please enter a directory of mp3 files")
$Mp3Files = (Get-ChildItem $MusicDir | Where-Object Extension -eq ".mp3") # Only finds mp3's in the directory

Write-Verbose "This might take a long time depending on the number of songs."

#Large ForEach loop to actually do everything one at a time to all files in the directory
$Mp3Files | ForEach-Object {
# Section 1, Grabbing values from mp3 file name

$FileName = $_.Name # Grab just the raw string data of just the file names.
$Artist = ($FileName -split " - ")[0]
$Title = ($FileName -split " - " -split "\.mp3")[1]

if ($FileName -like "*(feat *") # Checks for different combinations of featured and slices out the featured singer/artist
{ $Featured = (($FileName.split( ")" )[0]) -split "\(feat ")[1] }
elseif ($FileName -like "*(ft *") 
{ $Featured = (($FileName.split( ")" )[0]) -split "\(ft ")[1] }
elseif ($FileName -like "*(ft. *")
{ $Featured = (($FileName.split( ")" )[0]) -split "\(ft\. ")[1] }
elseif ($FileName -like "*(feat. *")
{ $Featured = (($FileName.split( ")" )[0]) -split "\(feat\. ")[1] }
else
{ $Featured = "No featured" }

if ($_ -like "*remix*")
{ $Remixer = ($FileName -split " remix")[0].Split( "(" )[1] }
else
{ $Remixer = "No remixer" }

# Section 2, setting metadata using values grabbed previously

$Song = [TagLib.File]::Create((Resolve-Path $_.fullname))

$Song.Tag.Title = $Title

if ($Featured -eq "No featured")
{ $Song.Tag.Artists = $Artist }
else
{ $Song.Tag.Artists = "$Artist, $Featured" }

if ($Remixer -eq "No remixer")
{ $Song.Tag.AlbumArtists = $Artist }
else
{ $Song.Tag.AlbumArtists = $Remixer }

$Song.Save() # Actually commits the changes to the mp3 file
} # End of for loop
